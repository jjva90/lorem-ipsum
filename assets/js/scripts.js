"use strict";

const btnGenerator = document.getElementById("generator");

console.log(btnGenerator);
const textContainer = document.querySelector(".ipsum-container");

const firstWord = "Lorem Ipsum";
const max = 27;
const min = 20;
const words = [
  "Twitter pemanly suspended Trump's account.",
  "Biden's inauguration",
  "Covid-19 Delta's variant ",
  "Pfizer, AstraZeneca, Moderna",
  "Olympic Games in Japan",
  "Jezz Bezos congratulates @SpaceX team on their successful Inspiration4 launch.",
  "Zack snyder justice league",
  "Friends reunion",
  "Black Widow",
  "A Quiet Place part II",
  "Luca silenzio Bruno",
  "WandaVision, it was Agatha all along.",
  "The Suicide Squad",
  "Space Jam a new legacy",
  "Oprah with Meghan and Harry",
  "Were you silent or were you silenced",
  "Prince Harry and Megan's accusations against Buckingham Palace.",
  "Fearless (Taylor's version)",
  "(Taylor's version)",
  "Kim Kardashian filing for divorce from Kanye West after seven years of marriage.",
  "Beyoncé the most-awarded woman in Grammy history",
  "cargo ship Ever Given stuck in the Suez Canal",
  "Rapper DaBaby apologises for HIV comments.",
  "Prince Philip dies at 99",
  "Chemtrails over the country club.",
  "Chromatica remix album",
  "Metelo-sacalo",
  "Ellen DeGeneres Show's final season",
  "Luisito Comunica bought a house in Venezuela",
  "$20,000 for a house in Venezuela",
  "Attack on Titan ",
  "Bitcoin",
  "Return to the office",
  "Free Britney",
  "Jeff Bezos blasts into space on own rocket",
];

btnGenerator.addEventListener("click", function () {
  let newWords = [firstWord];
  let paragraphsQtty = document.getElementById("paragraphs").value;
  textContainer.innerHTML = "";

  for (let i = 0; i < paragraphsQtty; i++) {
    const index = [
      ...new Set(
        Array.from({ length: Math.random() * (max - min) + min }, () =>
          Math.floor(Math.random() * words.length)
        )
      ),
    ];
    for (let j = 0; j < words.length; j++) {
      newWords.push(words[index[j]]);
    }
    textContainer.innerHTML += "<p>" + newWords.join(" ") + ".</p>";
    newWords = [];
  }

  console.log(paragraphsQtty);
});
